FreeCodeCamp.org is a free online Javascript/HTML/CSS course.

This is my attempt of the Product Landing Page project in their curriculum.

Read the project requirements:
https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-product-landing-page/

These source files are deployed here:
https://tonnerkiller.gitlab.io/FFC-Landing_Page
